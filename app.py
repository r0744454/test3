import flask
from flask import request, jsonify
from fastai.vision.all import *



app = flask.Flask(__name__)
our_out_of_the_box_model_inference = load_learner('export.pkl')
# let's test our model on an image
#our_out_of_the_box_model_inference = load_learner('models\first_model.pth')

#app.config["DEBUG"] = True
stringwaardepred = str(max(our_out_of_the_box_model_inference.predict('Mais/4/Pw4 (1).JPG')[2]))
# Create some test data for our catalog in the form of a list of dictionaries.
Output = {'output': int(our_out_of_the_box_model_inference.predict('Mais/4/Pw4 (1).JPG')[0]) -1,
        'accuracy': float(stringwaardepred[11:-1])*100
        }


@app.route('/', methods=['GET'])
def home():
    return jsonify(Output)
app.run()